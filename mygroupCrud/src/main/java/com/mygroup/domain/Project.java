package com.mygroup.domain;

import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;



@Entity
public class Project {

	
	@Id
	@GeneratedValue
	@Column(name = "project_id")
	private Long project_id;
	
	private String projectName;
	//bi-directional many-to-many association to Entite
	
	@ManyToMany
	@JoinTable(
		name="entiteproject"
		, joinColumns={
				
			@JoinColumn(name="project_project_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="entite_id_entite")
			}
		)
	private List<Entite> entites;

	//bi-directional many-to-many association to Fund
	@ManyToMany
	@JoinTable(
		name="fundproject"
		, joinColumns={
			@JoinColumn(name="project_project_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="fund_fund_id")
			}
		)
	private List<Fund> funds;
	
	public List<Entite> getEntites() {
		return entites;
	}

	public void setEntites(List<Entite> entites) {
		this.entites = entites;
	}

	public List<Fund> getFunds() {
		return funds;
	}

	public void setFunds(List<Fund> funds) {
		this.funds = funds;
	}

	public Long getProject_id() {
		return project_id;
	}

	public void setProject_id(Long project_id) {
		this.project_id = project_id;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}



	
}
