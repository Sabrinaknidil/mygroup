package com.mygroup.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Representative {
		
	@Id
	@GeneratedValue
	@Column(name = "representative_id")
	private Long representative_id;
	
	//additionnal field to the join table
	private String representativeTitle;
	
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="entite_id")
	private Entite entite;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="people_id")
	private People people;

	
	public Representative(String representativeTitle, Entite entite, People people) {
		super();
		this.representativeTitle = representativeTitle;
		this.entite = entite;
	this.people = people;
	}

	

	public Long getRepresentative_id() {
		return representative_id;
	}



	public void setRepresentative_id(Long representative_id) {
		this.representative_id = representative_id;
	}



	public String getRepresentativeTitle() {
		return representativeTitle;
	}

	public void setRepresentativeTitle(String representativeTitle) {
		this.representativeTitle = representativeTitle;
	}

	
	public Entite getEntite() {
		return entite;
	}

	public void setEntite(Entite entite) {
		this.entite = entite;
	}

	public People getPeople() {
		return people;
	}

	public void setPeople(People people) {
		this.people = people;
	}

}
