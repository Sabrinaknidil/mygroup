package com.mygroup.domain;

import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


@Entity
public class Fund {

	

	 @Override
	    public String toString(){
	        return this.fundName;
	    }
	 
	@Id
	@GeneratedValue
	@Column(name = "fund_id")
	private Long fund_id;
	@Column(name = "fundName")
	private String fundName;
	@Column(name = "groupeId")
    private Long groupeId;
	
	
	//bi-directional many-to-many association to Project
	@ManyToMany(mappedBy="funds")
	private List<Project> projects;
	public Fund() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getFund_id() {
		return fund_id;
	}


	public void setFund_id(Long fund_id) {
		this.fund_id = fund_id;
	}



	public List<Project> getProjects() {
		return projects;
	}



	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}



	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public Long getGroupeId() {
		return groupeId;
	}

	public void setGroupeId(Long groupeId) {
		this.groupeId = groupeId;
	}
	

}
