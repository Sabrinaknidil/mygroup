package com.mygroup.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.TemporalType;


@Entity
public class People {

	
	 @Override
	    public String toString(){
	        return this.firstName;
	    }
	 public Long affich(){
		 
		 return this.people_id;
	 }
	 @Id
	 @GeneratedValue
	 @Column(name="people_id")
	 private Long people_id;
	 
	 private String title;
	 private String firstName;
	 private String lastName;
	 @javax.persistence.Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_of_birth")
	 private Date dateOfBirth;
	 private String cityOfBirth;
	 private String CountryOfBirth;
	 private String Position;
	 private String adrNumber;
	 private String adrStreet;
	 private String adrZipCode;
	 private String adrCity;
	 private String adrProvinceState;
	 private String adrCountry;
	 private String passeportId;
	 private String email;
	private String phone;
	 
	 @OneToMany(mappedBy="people")
	 private List<Representative> representatives;
	 
	public List<Representative> getRepresentatives() {
		return representatives;
	}

	public void setRepresentatives(List<Representative> representatives) {
		this.representatives = representatives;
	}


	
	public People(String title, String firstName, String lastName) {
		super();
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public People () {}
	 public String getEmail() {
			return email;
		}


	public Long getPeople_id() {
		return people_id;
	}

	public void setPeople_id(Long people_id) {
		this.people_id = people_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCityOfBirth() {
		return cityOfBirth;
	}

	public void setCityOfBirth(String cityOfBirth) {
		this.cityOfBirth = cityOfBirth;
	}

	public String getCountryOfBirth() {
		return CountryOfBirth;
	}

	public void setCountryOfBirth(String countryOfBirth) {
		CountryOfBirth = countryOfBirth;
	}

	public String getPosition() {
		return Position;
	}

	public void setPosition(String position) {
		Position = position;
	}

	public String getAdrNumber() {
		return adrNumber;
	}

	public void setAdrNumber(String adrNumber) {
		this.adrNumber = adrNumber;
	}

	public String getAdrStreet() {
		return adrStreet;
	}

	public void setAdrStreet(String adrStreet) {
		this.adrStreet = adrStreet;
	}

	public String getAdrZipCode() {
		return adrZipCode;
	}

	public void setAdrZipCode(String adrZipCode) {
		this.adrZipCode = adrZipCode;
	}

	public String getAdrCity() {
		return adrCity;
	}

	public void setAdrCity(String adrCity) {
		this.adrCity = adrCity;
	}

	public String getAdrProvinceState() {
		return adrProvinceState;
	}

	public void setAdrProvinceState(String adrProvinceState) {
		this.adrProvinceState = adrProvinceState;
	}

	public String getAdrCountry() {
		return adrCountry;
	}

	public void setAdrCountry(String adrCountry) {
		this.adrCountry = adrCountry;
	}

	public String getPasseportId() {
		return passeportId;
	}

	public void setPasseportId(String passeportId) {
		this.passeportId = passeportId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	 
	
}
