package com.mygroup.domain;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.TemporalType;

@Entity
public class Movement {
	 @Override
	    public String toString(){
	        return this.operationType;
	    }
	@Id
	@GeneratedValue
	@Column(name="movement_id")
	private Long movement_id;
	private String operationType;
	

	private String effectiveDate;
	private String typeOfInterest;
	private String classOfInterest;
	private int numberOfInterest;
	private String currency;
	private BigDecimal bookValue;
	private BigDecimal aggBookValue;
	private BigDecimal interestRate;
	private String interestPeriod;
	private String interestCapitalisation;
	private int certificat;
	private String location;
	private int payment;
	private String paymentInKind;
	private BigDecimal interestPayment;
	private BigDecimal sumPrincipal;
	//bi-directional many-to-one association to Entite
		@ManyToOne
		@JoinColumn(name="entity_creditor_id")
		private Entite entite_creditor;

		//bi-directional many-to-one association to Entite
		@ManyToOne
		@JoinColumn(name="entity_debtor_id")
		private Entite entite_debtor;
	


	public Entite getEntite_creditor() {
			return entite_creditor;
		}
		public void setEntite_creditor(Entite entite_creditor) {
			this.entite_creditor = entite_creditor;
		}
		public Entite getEntite_debtor() {
			return entite_debtor;
		}
		public void setEntite_debtor(Entite entite_debtor) {
			this.entite_debtor = entite_debtor;
		}
	public Long getMovement_id() {
			return movement_id;
		}
		public void setMovement_id(Long movement_id) {
			this.movement_id = movement_id;
		}
	public String getOperationType() {
		return operationType;
	}
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getTypeOfInterest() {
		return typeOfInterest;
	}
	public void setTypeOfInterest(String typeOfInterest) {
		this.typeOfInterest = typeOfInterest;
	}
	public String getClassOfInterest() {
		return classOfInterest;
	}
	public void setClassOfInterest(String classOfInterest) {
		this.classOfInterest = classOfInterest;
	}
	public int getNumberOfInterest() {
		return numberOfInterest;
	}
	public void setNumberOfInterest(int numberOfInterest) {
		this.numberOfInterest = numberOfInterest;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getBookValue() {
		return bookValue;
	}
	public void setBookValue(BigDecimal bookValue) {
		this.bookValue = bookValue;
	}
	public BigDecimal getAggBookValue() {
		return aggBookValue;
	}
	public void setAggBookValue(BigDecimal aggBookValue) {
		this.aggBookValue = aggBookValue;
	}
	public BigDecimal getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}
	public String getInterestPeriod() {
		return interestPeriod;
	}
	public void setInterestPeriod(String interestPeriod) {
		this.interestPeriod = interestPeriod;
	}
	public String getInterestCapitalisation() {
		return interestCapitalisation;
	}
	public void setInterestCapitalisation(String interestCapitalisation) {
		this.interestCapitalisation = interestCapitalisation;
	}
	public int getCertificat() {
		return certificat;
	}
	public void setCertificat(int certificat) {
		this.certificat = certificat;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getPayment() {
		return payment;
	}
	public void setPayment(int payment) {
		this.payment = payment;
	}
	public String getPaymentInKind() {
		return paymentInKind;
	}
	public void setPaymentInKind(String paymentInKind) {
		this.paymentInKind = paymentInKind;
	}
	public BigDecimal getInterestPayment() {
		return interestPayment;
	}
	public void setInterestPayment(BigDecimal interestPayment) {
		this.interestPayment = interestPayment;
	}
	public BigDecimal getSumPrincipal() {
		return sumPrincipal;
	}
	public void setSumPrincipal(BigDecimal sumPrincipal) {
		this.sumPrincipal = sumPrincipal;
	}
	
	

}
