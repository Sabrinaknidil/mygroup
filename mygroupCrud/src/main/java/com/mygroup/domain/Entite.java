package com.mygroup.domain;
import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the entite database table.
 * 
 */
@Entity
@NamedQuery(name="Entite.findAll", query="SELECT e FROM Entite e")
public class Entite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_entite")
	private Long id_entite;

	@Column(name="adr_city")
	private String adrCity;

	@Column(name="adr_country")
	private String adrCountry;

	@Column(name="adr_number")
	private String adrNumber;

	@Column(name="adr_street")
	private String adrStreet;

	@Column(name="adr_zip_code")
	private String adrZipCode;

	private String auditor;

	private String currency;

	@Temporal(TemporalType.DATE)
	@Column(name="date_incorporation")
	private Date dateIncorporation;

	@Temporal(TemporalType.DATE)
	@Column(name="first_financial_year_end")
	private Date firstFinancialYearEnd;

	private String form;

	@Column(name="governing_law")
	private String governingLaw;

	private String name;

	@Column(name="registration_number")
	private String registrationNumber;

	@Column(name="share_capital")
	private int shareCapital;

	private String status;

	@Column(name="tax_number")
	private String taxNumber;

	@Column(name="us_tax_election")
	private String usTaxElection;

	@Column(name="vat_number")
	private String vatNumber;

	//bi-directional many-to-many association to Project
	@ManyToMany(mappedBy="entites")
	private List<Project> projects;

	//bi-directional many-to-one association to Movement
	@OneToMany(mappedBy="entite_creditor")
	private List<Movement> movement_creditor;

	//bi-directional many-to-one association to Movement
	@OneToMany(mappedBy="entite_debtor")
	private List<Movement> movement_debtor;

	//bi-directional many-to-one association to Representative
	@OneToMany(mappedBy="entite")
	private List<Representative> representatives;

	public Entite() {
	}



	public Long getId_entite() {
		return id_entite;
	}



	public void setId_entite(Long id_entite) {
		this.id_entite = id_entite;
	}



	public String getAdrCity() {
		return this.adrCity;
	}

	public void setAdrCity(String adrCity) {
		this.adrCity = adrCity;
	}

	public String getAdrCountry() {
		return this.adrCountry;
	}

	public void setAdrCountry(String adrCountry) {
		this.adrCountry = adrCountry;
	}

	public String getAdrNumber() {
		return this.adrNumber;
	}

	public void setAdrNumber(String adrNumber) {
		this.adrNumber = adrNumber;
	}

	public String getAdrStreet() {
		return this.adrStreet;
	}

	public void setAdrStreet(String adrStreet) {
		this.adrStreet = adrStreet;
	}

	public String getAdrZipCode() {
		return this.adrZipCode;
	}

	public void setAdrZipCode(String adrZipCode) {
		this.adrZipCode = adrZipCode;
	}

	public String getAuditor() {
		return this.auditor;
	}

	public void setAuditor(String auditor) {
		this.auditor = auditor;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getDateIncorporation() {
		return this.dateIncorporation;
	}

	public void setDateIncorporation(Date dateIncorporation) {
		this.dateIncorporation = dateIncorporation;
	}

	public Date getFirstFinancialYearEnd() {
		return this.firstFinancialYearEnd;
	}

	public void setFirstFinancialYearEnd(Date firstFinancialYearEnd) {
		this.firstFinancialYearEnd = firstFinancialYearEnd;
	}

	public String getForm() {
		return this.form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getGoverningLaw() {
		return this.governingLaw;
	}

	public void setGoverningLaw(String governingLaw) {
		this.governingLaw = governingLaw;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegistrationNumber() {
		return this.registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public int getShareCapital() {
		return this.shareCapital;
	}

	public void setShareCapital(int shareCapital) {
		this.shareCapital = shareCapital;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTaxNumber() {
		return this.taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public String getUsTaxElection() {
		return this.usTaxElection;
	}

	public void setUsTaxElection(String usTaxElection) {
		this.usTaxElection = usTaxElection;
	}

	public String getVatNumber() {
		return this.vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public List<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}


	public List<Movement> getMovement_creditor() {
		return movement_creditor;
	}

	public void setMovement_creditor(List<Movement> movement_creditor) {
		this.movement_creditor = movement_creditor;
	}

	public List<Movement> getMovement_debtor() {
		return movement_debtor;
	}

	public void setMovement_debtor(List<Movement> movement_debtor) {
		this.movement_debtor = movement_debtor;
	}

	public List<Representative> getRepresentatives() {
		return this.representatives;
	}

	public void setRepresentatives(List<Representative> representatives) {
		this.representatives = representatives;
	}


	
}