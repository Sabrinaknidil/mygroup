package com.mygroup.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mygroup.domain.Entite;

@Repository
public interface EntiteRepository extends CrudRepository<Entite,Long> {

}
