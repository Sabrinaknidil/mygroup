package com.mygroup.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.mygroup.domain.Movement;


@Repository
public interface MovementRepository extends CrudRepository<Movement, Long> {

}
