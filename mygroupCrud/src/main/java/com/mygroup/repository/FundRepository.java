package com.mygroup.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mygroup.domain.Fund;
@Repository
public interface FundRepository extends CrudRepository<Fund, Long>{

}
