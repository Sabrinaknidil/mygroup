package com.mygroup.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mygroup.dao.MovementDAO;
import com.mygroup.domain.Movement;
import com.mygroup.repository.MovementRepository;
@Service
public class MovementService {

	   @Autowired
	    private MovementDAO movementDAO;

	    @Autowired
	    private MovementRepository movementRepository;

	    public Object findAll(){
	        return movementRepository.findAll();
	    }

	    public Movement findById(Long id){
	      return movementRepository.findOne(id);
	    }

	    public Movement save(Movement movement){
	        return movementRepository.save(movement);
	    }
	    public void delete(Long id){
	    	movementRepository.delete(id);
	    	
	    }
}
