package com.mygroup.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mygroup.dao.EntiteDAO;
import com.mygroup.domain.Entite;
import com.mygroup.repository.EntiteRepository;

@Service
public class EntiteService {


	   @Autowired
	    private EntiteDAO entiteDAO;

	    @Autowired
	    private EntiteRepository entiteRepository;

	    public Object findAll(){
	        return entiteRepository.findAll();
	    }

	    public Entite findById(Long id){
	        return entiteRepository.findOne(id);
	    }

	    public Entite save(Entite entite){
	        return entiteRepository.save(entite);
	    }
	    public void delete(Long id){
	    	entiteRepository.delete(id);
	    	
	    }
}
