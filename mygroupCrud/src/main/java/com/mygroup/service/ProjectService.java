package com.mygroup.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mygroup.dao.ProjectDAO;
import com.mygroup.domain.Project;
import com.mygroup.repository.ProjectRepository;

@Service
public class ProjectService {


	   @Autowired
	    private ProjectDAO projectDAO;

	    @Autowired
	    private ProjectRepository projectRepository;

	    public Object findAll(){
	        return projectRepository.findAll();
	    }

	    public Project findById(Long id){
	        return projectRepository.findOne(id);
	    }

	    public Project save(Project entite){
	        return projectRepository.save(entite);
	    }
	    public void delete(Long id){
	    	projectRepository.delete(id);
	    	
	    }
}
