package com.mygroup.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mygroup.dao.FundDAO;
import com.mygroup.domain.Fund;
import com.mygroup.repository.FundRepository;

@Service
public class FundService {
	
	@Autowired
	private FundDAO fundDAO;
	
	@Autowired
	private FundRepository fundRepository;
	
	 public Object findAll(){
	        return fundRepository.findAll();
	    }

	    public Fund findById(Long id){
	    	
	        return fundRepository.findOne(id);
	    }

	    public Fund save(Fund fund){
	        return fundRepository.save(fund);
	    }
	   
	    
	    public void delete(Long id){
	    	
	    	fundRepository.delete(id);
	    }

}
