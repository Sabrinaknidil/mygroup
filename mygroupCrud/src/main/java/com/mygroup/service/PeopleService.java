package com.mygroup.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mygroup.dao.PeopleDAO;
import com.mygroup.domain.People;
import com.mygroup.repository.PeopleRepository;


@Service
public class PeopleService {

	
	   @Autowired
	    private PeopleDAO peopleDAO;

	    @Autowired
	    private PeopleRepository peopleRepository;

	    public Object findAll(){
	        return peopleRepository.findAll();
	    }

	    public People findById(Long id){
	        return peopleRepository.findOne(id);
	    }

	    public People save(People people){
	        return peopleRepository.save(people);
	    }
	 
	    public void delete(Long id){
	    	 peopleRepository.delete(id);
	    	
	    }
}
