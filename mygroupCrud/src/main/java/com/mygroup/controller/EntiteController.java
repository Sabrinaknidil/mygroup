package com.mygroup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mygroup.domain.Entite;
import com.mygroup.service.EntiteService;


@Controller
@RequestMapping(value = {"","/entite"})
public class EntiteController {

    @Autowired
    private EntiteService entiteService;


    @RequestMapping(value = {"/","index"})
    public String index(Model model){
        System.out.println(entiteService.findAll());
        model.addAttribute("entites", entiteService.findAll());
        return "entite/index";
    }

    @RequestMapping(value = "create")
    public String create(){
        return "entite/create";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(Entite entite){
        entiteService.save(entite);
        return "redirect:index";
    }

    @RequestMapping(value = "edit/{id}")
    public String edit(@PathVariable Long id, Model model){
        model.addAttribute("entite",entiteService.findById(id));
        return "entite/edit";
    }


    @RequestMapping(value = "update",method = RequestMethod.POST)
    public String update(Entite entite){
        entiteService.save(entite);
        return "redirect:index";
    }
    @RequestMapping(value = "delete/{id}")
    public String delete(@PathVariable Long id){
       
    	entiteService.delete(id);    
       
        return "redirect:/entite/index";
    }


    @RequestMapping(value = "/test")
    public String test(){
        return " hello world";
    }


}
