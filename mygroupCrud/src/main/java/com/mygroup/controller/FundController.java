package com.mygroup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mygroup.domain.Fund;
import com.mygroup.service.FundService;

@Controller
@RequestMapping(value = {"","/fund"})

public class FundController {



    @Autowired
    private FundService fundService;


    @RequestMapping(value = {"/","index"})
    public String index(Model model){
        System.out.println(fundService.findAll());
        model.addAttribute("funds", fundService.findAll());
        return "fund/index";
    }

    @RequestMapping(value = "create")
    public String create(){
        return "fund/create";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(Fund fund){
    	fundService.save(fund);
        return "redirect:index";
    }

    @RequestMapping(value = "edit/{id}")
    public String edit(@PathVariable Long id, Model model){
        model.addAttribute("fund",fundService.findById(id));
        return "fund/edit";
    }
    
    @RequestMapping(value = "delete/{id}")
    public String delete(@PathVariable Long id){
       
    	fundService.delete(id);    
       
        return "redirect:/fund/index";
    }


    @RequestMapping(value = "update",method = RequestMethod.POST)
    public String update(Fund fund){
    	fundService.save(fund);
        return "redirect:index";
    }


    @RequestMapping(value = "/test")
    public String test(){
        return " hello world";
    }


}

