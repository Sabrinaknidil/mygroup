package com.mygroup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mygroup.domain.Movement;
import com.mygroup.service.MovementService;

@Controller
@RequestMapping(value = {"","/movement"})
public class MovementController {
    @Autowired
    private MovementService movementService;


    @RequestMapping(value = {"/","index"})
    public String index(Model model){
        System.out.println(movementService.findAll());
        model.addAttribute("movements", movementService.findAll());
        return "movement/index";
    }

    @RequestMapping(value = "create")
    public String create(){
        return "movement/create";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(Movement movement){
        movementService.save(movement);
        return "redirect:index";
    }

    @RequestMapping(value = "edit/{id}")
    public String edit(@PathVariable Long id, Model model){
        model.addAttribute("movement",movementService.findById(id));
        return "movement/edit";
    }


    @RequestMapping(value = "update",method = RequestMethod.POST)
    public String update(Movement movement){
        movementService.save(movement);
        return "redirect:index";
    }
    @RequestMapping(value = "delete/{id}")
    public String delete(@PathVariable Long id){
       
    	movementService.delete(id);    
       
        return "redirect:/fund/index";
    }

    @RequestMapping(value = "/test")
    public String test(){
        return " hello world";
    }
}
