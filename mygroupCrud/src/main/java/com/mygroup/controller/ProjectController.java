package com.mygroup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mygroup.domain.Project;
import com.mygroup.service.ProjectService;


@Controller
@RequestMapping(value = {"","/project"})
public class ProjectController {

    @Autowired
    private ProjectService projectService;


    @RequestMapping(value = {"/","index"})
    public String index(Model model){
        System.out.println(projectService.findAll());
        model.addAttribute("projects", projectService.findAll());
        return "project/index";
    }

    @RequestMapping(value = "create")
    public String create(){
        return "project/create";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(Project project){
        projectService.save(project);
        return "redirect:index";
    }

    @RequestMapping(value = "edit/{id}")
    public String edit(@PathVariable Long id, Model model){
        model.addAttribute("project",projectService.findById(id));
        return "project/edit";
    }


    @RequestMapping(value = "update",method = RequestMethod.POST)
    public String update(Project project){
        projectService.save(project);
        return "redirect:index";
    }
    @RequestMapping(value = "delete/{id}")
    public String delete(@PathVariable Long id){
       
    	projectService.delete(id);    
       
        return "redirect:/project/index";
    }


    @RequestMapping(value = "/test")
    public String test(){
        return " hello world";
    }


}
