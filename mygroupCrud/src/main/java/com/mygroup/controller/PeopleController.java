package com.mygroup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mygroup.domain.People;
import com.mygroup.service.PeopleService;

@Controller
@RequestMapping(value = {"","/people"})
public class PeopleController {

    @Autowired
    private PeopleService peopleService;


    @RequestMapping(value = {"/","index"})
    public String index(Model model){
        System.out.println(peopleService.findAll());
        model.addAttribute("peoples", peopleService.findAll());
        return "people/index";
    }

    @RequestMapping(value = "create")
    public String create(){
        return "people/create";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(People people){
        peopleService.save(people);
        return "redirect:index";
    }

    @RequestMapping(value = "edit/{id}")
    public String edit(@PathVariable Long id, Model model){
        model.addAttribute("people",peopleService.findById(id));
        return "people/edit";
    }
    
    @RequestMapping(value = "delete/{id}")
    public String delete(@PathVariable Long id){
       
    	peopleService.delete(id);    
       
        return "redirect:index";
    }


    @RequestMapping(value = "update",method = RequestMethod.POST)
    public String update(People people){
        peopleService.save(people);
        return "redirect:index";
    }


    @RequestMapping(value = "/test")
    public String test(){
        return " hello world";
    }


}
