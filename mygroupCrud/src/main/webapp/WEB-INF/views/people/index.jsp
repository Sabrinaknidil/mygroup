<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
    <h2>List of Peoples</h2>
    <table class="table table-bordered">
        <tr>
            <th>S.No</th>
            <th>Title</th>
            <th>FirstName</th>
            <th>LastName</th>
            <th>CityOfBirth</th>
            <th>CountryOfBirth</th>
             <th>Position</th>
            <th>AdrNumber</th>
            <th>AdrStreet</th>
            <th>AdrZipCode</th>
             <th>AdrCity</th>
            <th>AdrProvinceState</th>
            <th>AdrCountry</th>
            <th>PasseportId</th>
             <th>Email</th>
            <th>Phone</th>
            <th>Action</th>
        </tr>
        <tbody>
            <c:forEach items="${peoples}" var="people" varStatus="itr">
                <tr>
                    <td>${itr.index+1}</td>
                    <td>${people.title}</td>
                    <td>${people.firstName}</td>
                    <td>${people.lastName}</td>
                    
                    <td>${people.cityOfBirth}</td>
                    <td>${people.countryOfBirth}</td>
                    <td>${people.position}</td>
                    <td>${people.adrNumber}</td>
                    <td>${people.adrStreet}</td>
                    <td>${people.adrZipCode}</td>
                    <td>${people.adrCity}</td>
                    <td>${people.adrProvinceState}</td>
                    <td>${people.adrCountry}</td>
                    <td>${people.passeportId}</td>
                    <td>${people.email}</td>
                    <td>${people.phone}</td>
                   
                    <td><a href="/people/edit/${people.people_id}" class="btn btn-warning">Edit</a>
             <a href="/people/delete/${people.people_id}" class="btn btn-warning">Delete</a>
                </tr>
            </c:forEach>
        </tbody>

    </table>
    <a href="/people/create" class="btn btn-success">Add People</a>

    <script type="application/javascript" src="js/jquery.js"></script>
    <script type="application/javascript" src="js/bootstrap.js"></script>


</body>
</html>