<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
<h2>Edit People</h2>
<form action="/people/update" method="post">
    <input type="hidden" name="people_id" value="${people.people_id}">
    <table class="table table-bordered">
        <tbody>
        <tr><th>Title</th><td><input type="text" name="title" required="required" value="${people.title}"></td></tr>
        <tr><th>FirstName</th><td><input type="text" name="firstName" required="required" value="${people.firstName}"></td></tr>
        <tr><th>LastName</th><td><input type="text" name="lastName" required="required" value="${people.lastName}"></td></tr>
   <!--      <tr><th>DateOfBirth</th><td><input type="Date" name="DateOfBirth" value="${people.dateOfBirth}"></td></tr> -->
        <tr><th>CityOfBirth</th><td><input type="text" name="cityOfBirth" required="required" value="${people.cityOfBirth}"></td></tr>
        <tr><th>CountryOfBirth</th><td><input type="text" name="countryOfBirth" required="required" value="${people.countryOfBirth}"></td></tr>
        <tr><th>Position</th><td><input type="text" name="position" required="required" value="${people.position}"></td></tr>
        <tr><th>AdrNumber</th><td><input type="text" name="adrNumber" required="required" value="${people.adrNumber}"></td></tr>
        <tr><th>AdrStreet</th><td><input type="text" name="adrStreet" required="required" value="${people.adrStreet}"></td></tr>
        <tr><th>AdrZipCode</th><td><input type="text" name="adrZipCode" required="required" value="${people.adrZipCode}"></td></tr>
        <tr><th>AdrCity</th><td><input type="text" name="adrCity" required="required" value="${people.adrCity}"></td></tr>
        <tr><th>AdrProvinceState</th><td><input type="text" name="adrProvinceState" required="required" value="${people.adrProvinceState}"></td></tr>
        <tr><th>AdrCountry</th><td><input type="text" name="adrCountry" required="required" value="${people.adrCountry}"></td></tr>
        <tr><th>PasseportId</th><td><input type="text" name="passeportId" required="required" value="${people.passeportId}"></td></tr>
        <tr><th>Email</th><td><input type="text" name="email" required="required" value="${people.email}"></td></tr>       
        <tr><th>Phone</th><td><input type="text" name="phone" required="required" value="${people.phone}"></td></tr>
        <tr><td colspan="2"><input type="submit" value="Edit People" class="btn btn-success"></tr>
        </tbody>
    </table>
</form>
<a href="/people/index" class="btn btn-success">Back</a>

<script type="application/javascript" src="js/jquery.js"></script>
<script type="application/javascript" src="js/bootstrap.js"></script>


</body>
</html>
