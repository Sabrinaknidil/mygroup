<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />
</head>
<body>


             
<h2>Create new People</h2>
<form action="/people/save" method="post">
    <table class="table table-bordered">
       <tbody>
               <tr><th>Title</th><td><input type="text" name="title" required="required"></td></tr>
       
         <tr><th>FirstName</th><td><input type="text" name="firstName" required="required"></td></tr>
        <tr><th>LastName</th><td><input type="text" name="lastName" required="required"></td></tr>
 <!--  <tr><th>DateOfBirth</th><td><input type="datetime" name="DateOfBirth"></td></tr> -->
        <tr><th>CityOfBirth</th><td><input type="text" name="cityOfBirth"></td></tr>
        <tr><th>CountryOfBirth</th><td><input type="text" name="countryOfBirth"></td></tr>
        <tr><th>Position</th><td><input type="text" name="position"></td></tr>
        <tr><th>AdrNumber</th><td><input type="text" name="adrNumber"></td></tr>
        <tr><th>AdrStreet</th><td><input type="text" name="adrStreet"></td></tr>
        <tr><th>AdrZipCode</th><td><input type="text" name="adrZipCode"></td></tr>
        <tr><th>AdrCity</th><td><input type="text" name="adrCity"></td></tr>
        <tr><th>AdrProvinceState</th><td><input type="text" name="adrProvinceState"></td></tr>
        <tr><th>AdrCountry</th><td><input type="text" name="adrCountry"></td></tr>
        <tr><th>PasseportId</th><td><input type="text" name="passeportId"></td></tr>
        <tr><th>Email</th><td><input type="text" name="email"></td></tr>       
        <tr><th>Phone</th><td><input type="text" name="phone"></td></tr>
        <tr><td colspan="2"><input type="submit" value="Add People" class="btn btn-success"></tr>
        </tbody>
    </table>
</form>
<a href="/people/index" class="btn btn-success">Back</a>

<script type="application/javascript" src="js/jquery.js"></script>
<script type="application/javascript" src="js/bootstrap.js"></script>


</body>
</html>