<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
<h2>Add new movement</h2>
<form action="/movement/save" method="post">
    <table class="table table-bordered">
       <tbody>
       
        <tr><th>operationType</th><td><input type="text" name="operationType" required="required"></td></tr>
        <tr><th>effectiveDate</th><td><input type="date" name="effectiveDate" required="required"></td></tr>
        <tr><th>typeOfInterest</th><td><input type="text" name="typeOfInterest" required="required"></td></tr>
    <!--    <tr><th>classOfInterest</th><td><input type="text" name="classOfInterest" required="required"></td></tr>
        <tr><th>numberOfInterest</th><td><input type="text" name="numberOfInterest" required="required"></td></tr>
        <tr><th>currency</th><td><input type="text" name="currency" required="required"></td></tr>-->  
        <tr><th>bookValue</th><td><input type="text" name="bookValue" required="required"></td></tr>
        <tr><th>aggBookValue</th><td><input type="text" name="aggBookValue" required="required"></td></tr>
        <tr><th>interestRate</th><td><input type="text" name="interestRate" required="required"></td></tr>
        <tr><th>interestPeriod</th><td><input type="text" name="interestPeriod" required="required"></td></tr>
        <tr><th> interestCapitalisation</th><td><input type="text" name=" interestCapitalisation" required="required"></td></tr>
        <tr><th>certificat</th><td><input type="text" name="certificat" required="required"></td></tr>
        <tr><th>location</th><td><input type="text" name="location" required="required"></td></tr>
        <tr><th>payment</th><td><input type="text" name="payment" required="required"></td></tr>
        <tr><th>paymentInKind</th><td><input type="text" name="paymentInKind" required="required"></td></tr>
        <tr><th>interestPayment</th><td><input type="text" name="interestPayment" required="required"></td></tr>
        <tr><th>sumPrincipal</th><td><input type="text" name="sumPrincipal"></td></tr>
    <tr><th>EntityCreditorId</th><td><input type="text" name="entite_creditor"></td></tr>
        <tr><th>EntityDebtorId</th><td><input type="text" name="entite_debtor"></td></tr>
        <tr><td colspan="2"><input type="submit" value="Add movement" class="btn btn-success"></tr>
        </tbody>
    </table>
</form>
<a href="/movement/index" class="btn btn-success">Back</a>

<script type="application/javascript" src="js/jquery.js"></script>
<script type="application/javascript" src="js/bootstrap.js"></script>


</body>
</html>