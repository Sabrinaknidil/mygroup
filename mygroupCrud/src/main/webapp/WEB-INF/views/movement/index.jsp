<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
    <h2>List of Operations</h2>
    <table class="table table-bordered">
        <tr>
            <th>S.No</th>
            <th>movementType</th>
            <th>effectiveDate</th>
            <th>typeOfInterest</th>
            <!-- <th>classOfInterest</th>
            <th>numberOfInterest</th>
            <th>currency</th>
            <th>bookValue</th>
            <th>aggBookValue</th>
            <th>interestRate</th>
            <th>interestPeriod</th>
            <th>interestCapitalisation</th>
            <th>certificat</th>
            <th>location</th>
            <th>payment</th> -->
            <th>paymentInKind</th>
            <th>interestPayment</th>
            <th>sumPrincipal</th>
           <th>EntityCreditorId</th>
            <th>EntityDebtorId</th>
     <th>Action</th>
        </tr>
        <tbody>
            <c:forEach items="${movements}" var="movement" varStatus="itr">
                <tr>
                    <td>${itr.index+1}</td>
                    <td>${movement.operationType}</td>
                    <td>${movement.effectiveDate}</td>
                    <td>${movement.typeOfInterest}</td>
                <%--     <td>${movement.classOfInterest}</td>
                    <td>${movement.numberOfInterest}</td>
                    <td>${movement.currency}</td>
                    <td>${movement.bookValue}</td>
                    <td>${movement.aggBookValue}</td>
                    <td>${movement.interestRate}</td>
                    <td>${movement.interestPeriod}</td>
                    <td>${movement.interestCapitalisation}</td>
                    <td>${movement.certificat}</td>
                    <td>${movement.location}</td>
                    <td>${movement.payment}</td> --%>
                    <td>${movement.paymentInKind}</td>
                    <td>${movement.interestPayment}</td>
                    <td>${movement.sumPrincipal}</td>
                   <td>${movement.entite_creditor}</td>
                    <td>${movement.entite_debtor}</td> 
                    <td><a href="/movement/edit/${movement.movement_id}" class="btn btn-warning">Edit</a>
             <a href="/movement/delete/${movement.movement_id}" class="btn btn-warning">Delete</a>
                </tr>
            </c:forEach>
        </tbody>

    </table>
    <a href="/movement/create" class="btn btn-success">Add Operation</a>

    <script type="application/javascript" src="js/jquery.js"></script>
    <script type="application/javascript" src="js/bootstrap.js"></script>


</body>
</html>