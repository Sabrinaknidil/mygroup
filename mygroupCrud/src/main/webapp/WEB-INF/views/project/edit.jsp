<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
<h2>Edit Project</h2>
<form action="/project/update" method="post">
    <input type="hidden" name="project_id" value="${project.project_id}">
    <table class="table table-bordered">
        <tbody>
        <tr><th>projectName</th><td><input type="text" name="projectName" required="required" value="${project.projectName}"></td></tr>
        <tr><td colspan="2"><input type="submit" value="Edit project" class="btn btn-success"></tr>
        </tbody>
    </table>
</form>
<a href="/project/index" class="btn btn-success">Back</a>

<script type="application/javascript" src="js/jquery.js"></script>
<script type="application/javascript" src="js/bootstrap.js"></script>


</body>
</html>
