<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
<h2>Edit Entity</h2>
<form action="/entite/update" method="post">
    <input type="hidden" name="id_entite" value="${entite.id_entite}">
    <table class="table table-bordered">
        <tbody>
        <tr><th>name</th><td><input type="text" name="name" required="required" value="${entite.name}"></td></tr>
        <tr><th>status</th><td><input type="text" name="status" required="required" value="${entite.status}"></td></tr>
        <tr><th>form</th><td><input type="text" name="form" required="required" value="${entite.form}"></td></tr>
        <%-- <tr><th>governingLaw</th><td><input type="text" name="governingLaw" required="required" value="${entite.governingLaw}"></td></tr>
        <tr><th>currency</th><td><input type="text" name="currency" required="required" value="${entite.currency}"></td></tr>
        <tr><th>shareCapital</th><td><input type="text" name="shareCapital" required="required" value="${entite.shareCapital}"></td></tr>
        <tr><th>adrNumber</th><td><input type="text" name="adrNumber" required="required" value="${entite.adrNumber}"></td></tr>
        <tr><th>adrStreet</th><td><input type="text" name="adrStreet" required="required" value="${entite.adrStreet}"></td></tr>
        <tr><th>adrZipCode</th><td><input type="text" name="adrZipCode" required="required" value="${entite.adrZipCode}"></td></tr>
        <tr><th>adrCity</th><td><input type="text" name="adrCity" required="required" value="${entite.adrCity}"></td></tr>
        <tr><th>adrCountry</th><td><input type="text" name="adrCountry" required="required" value="${entite.adrCountry}"></td></tr>
        <tr><th>registrationNumber</th><td><input type="text" name="registrationNumber" required="required" value="${entite.registrationNumber}"></td></tr>
        <tr><th>auditor</th><td><input type="text" name="auditor" required="required" value="${entite.auditor}"></td></tr>
        <tr><th>taxNumber</th><td><input type="text" name="taxNumber" required="required" value="${entite.taxNumber}"></td></tr>
        <tr><th>vatNumber</th><td><input type="text" name="vatNumber" required="required" value="${entite.vatNumber}"></td></tr>
        <tr><th>usTaxElection</th><td><input type="text" name="usTaxElection" value="${entite.usTaxElection}"></td></tr> --%>
   <!--      <tr><th>dateIncorporation</th><td><input type="date" name="dateIncorporation"  value="${entite.dateIncorporation}"></td></tr> -->
      <!--   <tr><th>firstFinancialYearEnd</th><td><input type="date" name="firstFinancialYearEnd"  value="${entite.firstFinancialYearEnd}"></td></tr> -->
        <tr><td colspan="2"><input type="submit" value="Edit People" class="btn btn-success"></tr>
        </tbody>
    </table>
</form>
<a href="/entite/index" class="btn btn-success">Back</a>

<script type="application/javascript" src="js/jquery.js"></script>
<script type="application/javascript" src="js/bootstrap.js"></script>


</body>
</html>