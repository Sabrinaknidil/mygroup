<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
<h2>Create new Entity</h2>
<form action="/entite/save" method="post">
    <table class="table table-bordered">
       <tbody>
        <tr><th>name</th><td><input type="text" name="name"></td></tr>
        <tr><th>status</th><td><input type="text" name="status"></td></tr>
        <tr><th>form</th><td><input type="text" name="form"></td></tr>
  <!--       <tr><th>governingLaw</th><td><input type="text" name="governingLaw"></td></tr>
        <tr><th>currency</th><td><input type="text" name="currency"></td></tr>
        <tr><th>shareCapital</th><td><input type="text" name="shareCapital"></td></tr>
        <tr><th>adrNumber</th><td><input type="text" name="adrNumber"></td></tr>
        <tr><th>adrStreet</th><td><input type="text" name="adrStreet"></td></tr>
        <tr><th>adrZipCode</th><td><input type="text" name="adrZipCode"></td></tr>
        <tr><th>adrCity</th><td><input type="text" name="adrCity"></td></tr>
        <tr><th>adrCountry</th><td><input type="text" name="adrCountry"></td></tr>
        <tr><th> registrationNumber</th><td><input type="text" name=" registrationNumber"></td></tr>
        <tr><th>auditor</th><td><input type="text" name="auditor"></td></tr>
        <tr><th>taxNumber</th><td><input type="text" name="taxNumber"></td></tr>
        <tr><th>vatNumber</th><td><input type="text" name="vatNumber"></td></tr>
        <tr><th>usTaxElection</th><td><input type="text" name="usTaxElection"></td></tr> -->
      <!--   <tr><th>dateIncorporation</th><td><input type="date" name="dateIncorporation" required="required"></td></tr>
        <tr><th>firstFinancialYearEnd</th><td><input type="date" name="firstFinancialYearEnd" required="required"></td></tr> -->
        <tr><td colspan="2"><input type="submit" value="Add Entity" class="btn btn-success"></tr>
        </tbody>
    </table>
</form>
<a href="/entite/index" class="btn btn-success">Back</a>

<script type="application/javascript" src="js/jquery.js"></script>
<script type="application/javascript" src="js/bootstrap.js"></script>


</body>
</html>