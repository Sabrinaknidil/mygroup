<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
    <h2>List of Entities</h2>
    <table class="table table-bordered">
        <tr>
            <th>S.No</th>
            <th>name</th>
            <th>status</th>
            <th>form</th>
            <th>governingLaw</th>
          <!--   <th>currency</th>
            <th>shareCapital</th>
            <th>adrNumber</th>
            <th>adrStreet</th>
            <th>adrZipCode</th>
            <th>adrCity</th>
            <th>adrCountry</th>
            <th>registrationNumber</th>
            <th>auditor</th>
            <th>taxNumber</th>
            <th>vatNumber</th> -->
            <th>sTaxElection</th>
            <th>dateIncorporationr</th>
            <th>firstFinancialYearEnd</th>
            <th>Action</th>
        </tr>
        <tbody>
            <c:forEach items="${entites}" var="entite" varStatus="itr">
                <tr>
                    <td>${itr.index+1}</td>
                    <td>${entite.name}</td>
                    <td>${entite.status}</td>
                    <td>${entite.form}</td>
                    <td>${entite.governingLaw}</td>
                   <%--  <td>${entite.currency}</td>
                    <td>${entite.shareCapital}</td>
                    <td>${entite.adrNumber}</td>
                    <td>${entite.adrStreet}</td>
                    <td>${entite.adrZipCode}</td>
                    <td>${entite.adrCity}</td>
                    <td>${entite.adrCountry}</td>
                    <td>${entite.registrationNumber}</td>
                    <td>${entite.auditor}</td>
                    <td>${entite.taxNumber}</td>
                    <td>${entite.vatNumber}</td> --%>
                    <td>${entite.usTaxElection}</td>
                    <td>${entite.dateIncorporation}</td>
                    <td>${entite.firstFinancialYearEnd}</td>
                    <td><a href="/entite/edit/${entite.id_entite}" class="btn btn-warning">Edit</a>
             <a href="/entite/delete/${entite.id_entite}" class="btn btn-warning">Delete</a>
                </tr>
            </c:forEach>
        </tbody>

    </table>
    <a href="/entite/create" class="btn btn-success">Add Entity</a>

    <script type="application/javascript" src="js/jquery.js"></script>
    <script type="application/javascript" src="js/bootstrap.js"></script>


</body>
</html>